package com.example.performance;

import com.example.performance.entity.second.PaymentTransaction2;
import com.example.performance.util.HibernateUtil;
import com.example.performance.entity.first.PaymentTransaction;
import org.hibernate.Session;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.RunnerException;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("ALL")
public class BenchmarkTest {

    public void init(){}

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = 1, timeUnit = TimeUnit.SECONDS)
    @Measurement(iterations = 1, timeUnit = TimeUnit.SECONDS)
    @Fork(value = 1, warmups = 2)
    public static void perfomance_without_getting_payload_from_related_table() {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            List<PaymentTransaction> paymentTransactionList =
                    (List<PaymentTransaction>)session.createQuery("FROM PaymentTransaction")
                            .getResultList();
            System.out.println(paymentTransactionList.size());
        }
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = 1, timeUnit = TimeUnit.SECONDS)
    @Measurement(iterations = 1, timeUnit = TimeUnit.SECONDS)
    @Fork(value = 1, warmups = 2)
    public static void perfomance_with_getting_payload_from_related_table() {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            List<PaymentTransaction> paymentTransactionList =
                    (List<PaymentTransaction>)session.createQuery("FROM PaymentTransaction")
                            .getResultList();
            System.out.println(paymentTransactionList.size());
            for (PaymentTransaction paymentTransaction : paymentTransactionList) {
                trick(paymentTransaction);
            }
        }
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @Warmup(iterations = 1, timeUnit = TimeUnit.SECONDS)
    @Measurement(iterations = 1, timeUnit = TimeUnit.SECONDS)
    @Fork(value = 1, warmups = 2)
    public static void perfomance_column_in_the_same_table() {
            try (Session session = HibernateUtil.getSessionFactory().openSession()) {
                List<PaymentTransaction2> paymentTransactionList =
                        (List<PaymentTransaction2>)session.createQuery("FROM PaymentTransaction2")
                                .getResultList();
                System.out.println(paymentTransactionList.size());
                for (PaymentTransaction2 paymentTransaction : paymentTransactionList) {
                    trick(paymentTransaction);
                }
            }
    }

    public static void main(String[] args) throws IOException, RunnerException {
        org.openjdk.jmh.Main.main(args);
    }


    public static void trick(PaymentTransaction transaction) {
        System.out.println(transaction.getId());
        System.out.println(transaction.getPayloadRef().getLargeBulkPayload());
    }

    public static void trick(PaymentTransaction2 transaction) {
        System.out.println(transaction.getId());
        System.out.println(transaction.getSmartPayload());
    }

}

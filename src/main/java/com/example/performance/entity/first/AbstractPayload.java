package com.example.performance.entity.first;

import javax.persistence.*;

/**
 * @author APapok <br>
 *         Copyright 2019 Earthport Plc. All rights reserved.
 */
@MappedSuperclass
public class AbstractPayload {

    public static final int MAX_PAYLOAD_SIZE = 7168;
    public static final int MAX_BULK_PAYLOAD_SIZE = 1048576;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "payload", length = MAX_PAYLOAD_SIZE)
    private String payload;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "large_bulk_payload", length = MAX_BULK_PAYLOAD_SIZE)
    private String largeBulkPayload;

    protected AbstractPayload() {
        super();
    }

    protected AbstractPayload(String payload) {
        this.payload = payload;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getLargeBulkPayload() {
        return largeBulkPayload;
    }

    public void setLargeBulkPayload(String largeBulkPayload) {
        this.largeBulkPayload = largeBulkPayload;
    }

    public String toString() {
        return payload;
    }

}

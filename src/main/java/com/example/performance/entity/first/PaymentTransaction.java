package com.example.performance.entity.first;

import javax.persistence.*;

/**
 * Payment transaction entity.
 *
 * @author AZaripov <br>
 *         Copyright 2017 Earthport Plc. All rights reserved.
 */
@Entity
@Table(name = "epa_payment_message_test")
public class PaymentTransaction extends AbstractPayloadManager<PaymentTransactionPayload> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    public Long getId() {
        return id;
    }

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "payload_id", unique = false, nullable = true)
    protected PaymentTransactionPayload payloadRef;

    @Override
    public PaymentTransactionPayload getPayloadRef() {
        return payloadRef;
    }

    @Override
    public void setPayloadRef(PaymentTransactionPayload payloadRef) {
        this.payloadRef = payloadRef;
    }

    @Override
    protected void updatePayloadRefWithShortPayload() {
        payloadRef = isSkipPayloadPersistence(payload) ? null : new PaymentTransactionPayload(payload);
    }

    @Override
    protected void processPayload() {
        setPayloadRef(new PaymentTransactionPayload());
        getPayloadRef().setLargeBulkPayload(largeBulkPayload);
    }

    public int hashCode() {
        return getId() == null ? 17 : getId().hashCode();
    }

    public boolean equals(Object obj) {
        return obj instanceof PaymentTransaction && (getId() == null ?
                obj == this : getId().equals(((PaymentTransaction) obj).getId()));
    }
}

package com.example.performance.entity.first;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Payload entity for {@link PaymentTransaction}.
 *
 * @author APapok <br>
 *         Copyright 2017 Earthport Plc. All rights reserved.
 */
@Entity
@Table(name = "epa_payment_message_payload_test")
public class PaymentTransactionPayload extends AbstractPayload {

    public PaymentTransactionPayload() {
    }

    public PaymentTransactionPayload(String payload) {
        super(payload);
    }

    public int hashCode() {
        return getId() == null ? 17 : getId().hashCode();
    }

    public boolean equals(Object obj) {
        return obj instanceof PaymentTransactionPayload && (getId() == null ?
                obj == this : getId().equals(((PaymentTransactionPayload) obj).getId()));
    }
}

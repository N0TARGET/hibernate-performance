package com.example.performance.entity.first;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.nio.charset.Charset;

/**
 *
 * This class represents a logic of defining size of payload
 *  and what column to choose in Payload table to save data.
 *
 * @author APapok <br>
 *         Copyright 2019 Earthport Plc. All rights reserved.
 */
@MappedSuperclass
public abstract class AbstractPayloadManager<PAYLOAD extends AbstractPayload> {

    private static final Log LOG = LogFactory.getLog(AbstractPayloadManager.class);

    @Transient
    protected String payload;

    @Transient
    protected String largeBulkPayload;

    public String getPayload() {
        if (largeBulkPayload != null) {
            return largeBulkPayload;
        }
        if (payload != null) {
            return payload;
        }
        payload = getPayloadRef() == null ? null : getPayloadRef().getPayload();
        if (payload == null) {
            largeBulkPayload = getPayloadRef() == null ? null : getPayloadRef().getLargeBulkPayload();
            return largeBulkPayload;
        }
        return payload;
    }

    public void setPayload(String payload) {
        if (isLargePayload(payload)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Setting large payload [" + largeBulkPayload + "] for the message: " + this.toString());
            }
            this.largeBulkPayload = payload;
            updatePayloadRefWithLargePayload();
        } else {
            this.payload = payload;
            updatePayloadRefWithShortPayload();
        }
    }

    private boolean isLargePayload(String payload) {
        return payload != null && getPayloadLength(payload) > AbstractPayload.MAX_PAYLOAD_SIZE;
    }

    private int getPayloadLength(String payload) {
        int payloadLength = payload.getBytes(Charset.forName("UTF-8")).length;
        if (LOG.isDebugEnabled()) {
            LOG.debug("Length of payload [" + payload + "] is " + payloadLength + " for the message: " + this.toString());
        }
        return payloadLength;
    }

    /**
     * Update payloadRef entity with large payload
     */
    private void updatePayloadRefWithLargePayload() {
        if (isSkipPayloadPersistence(largeBulkPayload)) {
            setPayloadRef(null);
        } else {
            int payloadLength = getPayloadLength(largeBulkPayload);
            if (payloadLength > AbstractPayload.MAX_BULK_PAYLOAD_SIZE) {
                LOG.error("Payload length for the message + " + this.toString() + " is greater than allowed by database limits. "
                        + "It won't be stored to database. Refer to a message backup file to view the payload.");
                setPayloadRef(null);
            } else {
                processPayload();
            }
        }
    }

    protected boolean isSkipPayloadPersistence(String payload) {
        return payload == null || payload.isEmpty();
    }

    public void setNewPayload(String payload) {
        setPayloadRef(null);
        setPayload(payload);
    }

    protected abstract void processPayload();

    protected abstract void updatePayloadRefWithShortPayload();

    protected abstract PAYLOAD getPayloadRef();

    protected abstract void setPayloadRef(PAYLOAD payloadRef);

}

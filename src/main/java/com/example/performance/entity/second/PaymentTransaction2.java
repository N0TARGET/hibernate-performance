package com.example.performance.entity.second;

import com.example.performance.entity.first.AbstractPayload;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.persistence.*;

import static com.example.performance.entity.first.AbstractPayload.MAX_BULK_PAYLOAD_SIZE;
import static com.example.performance.entity.first.AbstractPayload.MAX_PAYLOAD_SIZE;

/**
 * Payment transaction entity.
 *
 * @author AZaripov <br>
 *         Copyright 2017 Earthport Plc. All rights reserved.
 */
@Entity
@Table(name = "epa_payment_message_test2")
public class PaymentTransaction2  {

    private static final Log LOG = LogFactory.getLog(PaymentTransaction2.class);

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    public Long getId() {
        return id;
    }

    @Column(name = "payload", length = MAX_PAYLOAD_SIZE)
    private String payload;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    @Column(name = "large_bulk_payload", length = MAX_BULK_PAYLOAD_SIZE)
    private byte[] largeBulkPayload;

    private String getPayload() {
        return payload;
    }

    private String getLargeBulkPayload() {
        return new String(largeBulkPayload);
    }

    private boolean isLargePayload(String payload) {
        return payload != null && payload.length() > AbstractPayload.MAX_PAYLOAD_SIZE;
    }

    public String getSmartPayload() {
        String payload = getPayload();
        if (payload != null) {
            return payload;
        } else {
            return getLargeBulkPayload();
        }
    }

    public void setPayload(String payload) {
        if (isLargePayload(payload)) {
            this.largeBulkPayload = payload.getBytes();
            return;
        }
        this.payload = payload;
    }

    public int hashCode() {
        return getId() == null ? 17 : getId().hashCode();
    }

    public boolean equals(Object obj) {
        return obj instanceof PaymentTransaction2 && (getId() == null ?
                obj == this : getId().equals(((PaymentTransaction2) obj).getId()));
    }
}

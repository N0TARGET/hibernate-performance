package com.example.performance.util;


import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnectionTest {

    public static void main (String args[]) {

        String url = "jdbc:db2://localhost:60000/PAY";
        try {
            // Загрузка jdbc-odbc-bridge драйвера
            Class.forName("com.ibm.db2.jcc.DB2Driver");

            // Попытка соединения с драйвером. Каждый из
            // зарегистрированных драйверов будет загружаться, пока
            // не будет найден тот, который сможет обработать этот URL

            Connection con = DriverManager.getConnection(url, "dbbuild", "wantbeer");

            // Если соединиться не удалось, то произойдет exception (исключительная ситуация).

            // Получить DatabaseMetaData объект и показать информацию о соединении

            DatabaseMetaData dma = con.getMetaData();

            // Печать сообщения об успешном соединении
            System.out.println("\nConnected to " + dma.getURL());
            System.out.println("Driver " + dma.getDriverName());
            System.out.println("Version " + dma.getDriverVersion());

            // Закрыть соединение
            con.close();
        } catch (SQLException e) {
            System.out.println("\n*** SQLException caught ***\n");
            while (e != null) {
                System.out.println("SQLState: " + e.getSQLState());
                System.out.println("Message: " + e.getMessage());
                System.out.println("Vendor: " + e.getErrorCode());
                e = e.getNextException();
            }
        } catch (java.lang.Exception ex) {
            ex.printStackTrace();
        }

    }
}

package com.example.performance.util;

import com.example.performance.entity.first.PaymentTransaction;
import com.example.performance.entity.second.PaymentTransaction2;
import org.apache.commons.io.IOUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class DatabaseInit {

    public static final int BATCH_SIZE = 10_000;
    public static final int COUNT = 300_000;

    public static void main(String[] args) throws IOException {

        final String payload = fileFromResourcesAsString("samples/largePayload.txt");

        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            // start a transaction
            transaction = session.beginTransaction();
            // save the student objects
            for (int i = 0; i < COUNT; i++) {

                if (i > 0 && i % BATCH_SIZE == 0) {
                    transaction.commit();
                    transaction.begin();

                    session.clear();
                }

                PaymentTransaction paymentTransaction1 = new PaymentTransaction();
                paymentTransaction1.setPayload(payload);
                session.persist(paymentTransaction1);

                PaymentTransaction2 paymentTransaction2 = new PaymentTransaction2();
                paymentTransaction2.setPayload(payload);
                session.persist(paymentTransaction2);
            }

            // commit transaction
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    private static String fileFromResourcesAsString(String path) throws IOException {
        ClassLoader classLoader = DatabaseInit.class.getClassLoader();
        InputStream resourceAsStream = classLoader.getResourceAsStream(path);
        return IOUtils.toString(resourceAsStream, StandardCharsets.UTF_8);
    }

}

package com.example.reactive;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Mode;

public class EmployeeTest {

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @Fork(value = 1, warmups = 2)
    public void init() {
        // Do nothing
    }

        public static void main(String[] args) throws Exception {
            org.openjdk.jmh.Main.main(args);
    }

}